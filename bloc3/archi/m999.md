---
title: M999 - une architecture von Neumann
subtitle : Bloc 3
author: |
    | Département informatique, Univ. Lille
    | DIU Enseigner l'informatique au lycée
date: juillet 2019
---

> _sur la base de ressources cc-by-sa Philippe Marquet, Martin Quinson
> «M999, le processeur débranché»,_
> [`github.com/InfoSansOrdi/M999`](http://github.com/InfoSansOrdi/M999)

M-10 la petite machine débranchée
=================================

M-10 est une petite machine informatique constituée

* d'une mémoire qui contient des données, 
* d'une UAL, unité arithmétique et logique capable de réaliser une opération telle une addition,
* d'un accumulateur qui recevra le résultat des opérations réalisées par l'UAL. 

La mémoire est composée de 10 "cases". Ces cases sont numérotées de 0 à 9. Ce numéro est l'adresse de la case.

![](m10-0.png)

Jeu d'instructions
------------------

La petite machine M-10 dispose de deux types d'instructions :

* les instructions arithmétiques et logiques réalisées par l'UAL,
* les instructions de transfert entre la mémoire et l'accumulateur.

Les **instructions arithmétiques et logiques** sont les classiques opérations telles l'addition, la multiplication, ou l'égalité, ou encore le "et logique"... 
Ces opérations prennent leurs opérandes en mémoire.
Ces opérandes sont désignées par leur adresse.
Les instructions produisent leur résultat dans l'accumulateur.

Ainsi, l'instruction d'addition `ADD 1 2` va additionner les deux valeurs des "cases" mémoires correspondantes (case d'adresse `1` et case d'adresse `2`) aux adresses indiquées et produire le résultat dans l'accumulateur.

Les instructions `SUB`, `MUL`, `DIV` fonctionnent de la même manière et produisent respectivement la différence, le produit et la division.

Il existe (accessoirement) des opérations `LSS @1 @2`

* plus petit que
* idem pour `EQU`, `NEQ`, `LEQ`, `GTR`, `GEQ`

Les **instructions de transfert mémoire** permettent de copier la valeur de l'accumulateur vers une case mémoire, ou de copier la valeur d'une case mémoire vers l'accumulateur.
Ces instructions nécessitent donc un opérateur, l'adresse de la case mémoire.

L'instruction `LOAD` transfert de la mémoire vers l’accumulateur. L'instruction `STORE` transfert de l’accumulateur vers la mémoire.

→ Un premier programme
----------------------

Il s'agit de programmer un "programme de calcul" sur M-10.
Par exemple : 

* choisir un nombre
* soustraire 2 à ce nombre
* multiplier le résultat par 4

Et donc d'écrire une suite d'instructions de M-10 dont l'exécution correspondra à ce programme de calcul.

Essayez !

On en vient à se poser les questions 

* où est le nombre choisi ?
* où est le résultat ?
* comment désigner la valeur 2 ? 

et à y répondre :

* à nous de décider !

M999 le processeur (débranché)
==============================

Le fonctionnement du processeur M999 est caractéristique des
architectures dites _von Neumann_.

M999 est constitué :

* d'une mémoire qui contient à la fois les données et le programme
* d'une unité arithmétique et logique – UAL, _ALU_ en anglais – en
  charge de réaliser les opérations telles addition, comparaison...
* d'une unité de commande – ou unité de contrôle – qui pilote
  l'ordinateur 
* de dispositifs d’entrée-sortie. 

Mémoire et registres
--------------------

La mémoire est composée de 100 mots mémoire de 3 chiffres (valeur de
000 à 999). Ces 100 mots mémoire sont adressables par des adresses
codées sur 2 chiffres.

Cette mémoire va contenir données et instructions.

Le processeur dispose de deux registres généraux A et B, et d'un
registre accumulateur/résultat R.

Comme la mémoire, ces registres sont de 3 chiffres, et contiennent donc
des valeurs entre 0 et 999.

Le processeur dispose aussi d'un registre pointeur d'instruction PC
contenant l'adresse mémoire de la prochaine instruction à exécuter.

La mémoire et les registres peuvent être matérialisés par une grille
de 10*10 et des cases supplémentaires pour les registres A, B, R.

Le registre PC peut être matérialisé par un "pion" situé sur une des
cases de la grille mémoire.

![](m999-memoire.png)

Unité arithmétique et logique
-----------------------------

L'unité arithmétique et logique est en charge d'effectuer les
calculs. Les opérandes et résultats sont dans les registres, A et
B pour les opérandes, R pour le résultat.

Unité de commande
-----------------

L'unité de commande pilote l'ordinateur.

Son cycle de fonctionnement comporte 3 étapes :

1. charger l'instruction depuis la mémoire pointée par PC.
   Incrémenter PC.
2. décoder l'instruction : à partir des 3 chiffres codant
   l'instruction, identifier quelle est l’opération à réaliser,
   quelles sont les opérandes.
3. exécuter l'instruction.


Jeu d'instruction
-----------------

_(pour débuter)_

op0 | op1 op2 | mnémonique | instruction à réaliser
--- | ------- | ---------- | ----------------------
0 | _addr_ | `LDA`| copie le mot mémoire d’adresse _addr_ dans le registre A
1 | _addr_ | `LDB`| copie le mot mémoire d’adresse _addr_ dans le registre B
2 | _addr_ | `STR`| copie le contenu du registre R dans le mot mémoire d'adresse _addr_
3 | - - | – | **opérations arithmétiques et logiques**
3 | 0 0 | `ADD`| ajoute les valeurs des registres A et B, produit le résultat dans R 
3 | 0 1 | `SUB`| soustrait la valeur du registre B à celle du registre A, produit le résultat dans R 
3 | . . | etc | … 
3 | 9 9 | `NOP` | ne fait rien
4 | _rs_ _rd_ | `MOV` | copie la valeur du registre source _rs_ dans le registre destination _rd_.
5 | _addr_ | `JMP` | branche en _addr_ (PC reçoit la valeur _addr_) 
6 | _addr_ | `JPP` | branche en _addr_ si la valeur du registre R est strictement positive

Les registres sont désignés par les valeurs suivantes :

valeur | registre
------ | --------
0 | A
1 | B
2 | R

### Boot et arrêt

La machine démarre avec la valeur nulle comme pointeur d'instruction.

La machine stoppe si le pointeur d'instruction vaut 99.

On peut donc utiliser le mnémonique `HLT` comme synonyme de `JMP 99`.

### Entrées/sorties

Les entrées/sorties sont "mappées" en mémoire.

Écrire le mot mémoire 99 écrit sur le terminal.

Les valeurs saisies sur le terminal seront lues dans le mot mémoire 99.

Exécuter des programmes M999
============================

> _le fichier [m999-exos.asm](m999-exos-pasaran.md) contient les codes
assembleurs et les (éléments de) solutions des exercices_

* fonctionnement de M999
* cycle charger / décoder / exécuter 
* encodage et mnémonique 

### → 1re exécution ###

Soit l'état suivant de la mémoire (on pourra utiliser le fichier
[m999-exo1.pdf](m999-exo1.pdf)) :

```
       00    01    02 …
     +-----+-----+---
   0 | 399 | 599 | … 
     +-----+-----+---
   1 | 011 | 123 | … 
     +-----+-----+---
   2 | 112 | 042 | … 
     +-----+-----+---
   3 | 301 | …   | … 
     +-----+-----+---
   4 | 608 | …   | … 
     +-----+-----+---
   5 | 402 | …   | … 
     +-----+-----+---
   6 | 299 | …   | … 
     +-----+-----+---
   7 | 599 | …   | … 
     +-----+-----+---
   8 | 412 | …   | … 
     +-----+-----+---
   9 | 299 | …   | … 
     +-----+-----+---
```

Que provoque la mise en route de la machine ?

### → 2e exécution ###

Qu'en est-il si l'état de la mémoire est le suivant (les valeurs de
01 à 012 sont identiques au cas précédent) :

```
       00    01    02 …
     +-----+-----+---
   0 | 514 | 599 | 501 
     +-----+-----+---
   1 | 011 | 123 | … 
     +-----+-----+---
   2 | 112 | 042 | … 
     +-----+-----+---
   3 | 301 | 531 | … 
     +-----+-----+---
   4 | 608 | 099 | … 
     +-----+-----+---
   5 | 402 | 402 | … 
     +-----+-----+---
   6 | 299 | 211 | … 
     +-----+-----+---
   7 | 599 | 099 | … 
     +-----+-----+---
   8 | 412 | 402 | … 
     +-----+-----+---
   9 | 299 | 212 | … 
     +-----+-----+---
```

### → 1re programmation ###

Écrire un programme M999 qui affiche le minimum de 2 entiers saisis au
clavier.

Traduire ce programme en «binaire-décimal».

Quelle modification apporter si on désire saisir 3 entiers ? $`N`$
entiers ?


Programmer M999
===============

### → Produit de 2 entiers ###

Écrire un programme M999 de calcul du produit de deux entiers (on ne
dispose pas de l'opération de multiplication). 

On pourra procéder par étape :

* proposer une écriture dans un langage de "haut niveau" ad hoc
* traduire en mnémonique M999
  - sans se soucier des valeurs des adresses des variables et
	constantes - que l'on notera par exemple `@a` ou `@1` - 
  - sans se soucier des adresses des instructions - on utilisera par
    exemple des labels notés `label:` - 
* allocation des variables, identification des numéro des instructions
* étape finale de traduction en «binaire-décimal»	

### Pour s'entraîner ###

Proposer des codes M999 pour calculer :

* le produit non-nul de 2 entiers $`i`$ et $`j`$, défini par :
  * ($`(0,0) → 0`$)
  * $`(i,0) → i`$ 
  * $`(0,j) → j`$ 
  * $`(i,j) → i \times j`$ 


Plus de registres (bonus)
-----------------

L'exécution du programme précédent est pénalisé par le nombre et le
coût des transferts mémoire (le coût d'un accès mémoire est (au moins)
d'un ordre de grandeur supérieur à celui d'un accès registre).

Une évolution de M999 propose donc 

* l'ajout de registres génériques R3, R4, et R5
* l'ajout de registres constants pour les valeurs 0 et 1

L'instruction `MOV`

* désigne les registres R3, R4 et R5 par les valeurs 3, 4, et 5 !
* accède en lecture aux registres contenant 0 et 1 par les valeurs
  6 et 7 

(On peut envisager la possibilité d'échanger la valeur d'un registre
avec R, resp. R3, R4, R5 en utilisant les valeurs 6, resp. 7,
8, 9 pour désigner le registre destination. Ainsi `MOV 2 7` échange
les valeurs des registres B et R3.)

### → Produit avec des registres ###

Proposer une nouvelle version du calcul du produit de deux entiers. 

Réutilisation
-------------

### → $`x^2 = x \times x`$ ###

Comment définir un code du calcul du carré d'un entier utilisant le
code de calcul du produit ?

Deux aspects sont à considérer :

* un "_passage des paramètres_" et récupération de la valeur de retour
du produit. Qui peut être réalisé 
  * via les registres, par exemple A et B pour les paramètres, et
	R pour la valeur renvoyée. \
	Ne fonctionne que parce que le nombre de paramètres est
	réduit. \ 
	Nécessite de stocker ces valeurs en mémoire (ou dans d'autres 
	registres pour les utiliser dans la fonction)
  * via des mots mémoires réservés, par exemple les quelques mots
    mémoire qui précèdent les instructions. \
	Solution plus générale. 
* Gestion du "_return_" : une fois le produit réalisé, comment
  poursuivre l'exécution ?
  * on ne touche pas nécessairement le problème s'il n'y a rien
    à faire une fois le carré calculé

### → $`x^3 = x^2 \times x`$ ###

Comment définir maintenant un code du calcul du cube d'un entier
utilisant le code de calcul du produit et du carré.

Deux utilisations distinctes, deux "appels" seront fait au code de
calcul du produit. La nécessité de pouvoir reprendre l'exécution à la
suite de l'instruction "appelante" est manifeste. 


Appel et retour
---------------

Afin de réaliser des appels de procédure (on parle de _procédure_ et
non de fonction car le support matériel ne prend pas - pas encore - en
charge les paramètres et valeur de retour. On parlera de _fonction_
plus tard).

* on ajoute un registre SP, sommet de pile - _stack pointer_
* on introduit deux nouvelles instructions, `CAL` et `RET`

Le registre SP contient une adrese mémoire. Il est initialisé à 98.

Les instructions `CAL` et `RET` sont codées ainsi :

op0 | op1 op2 | mnémonique | instruction à réaliser
--- | ------- | ---------- | ----------------------
7   | _addr_  | `CAL`      | copie la valeur de PC dans la case mémoire SP ; décrémente SP ; copie _addr_ dans PC
3   | 9 8     | `RET`      | incrémente SP ; copie la valeur de la case mémoire SP dans PC 


(on notera que l'exécution de `CAL` comme de toutes les instructions,
est réalisée après que la valeur de PC ait été incrémentée.)

`CAL` permet ainsi de brancher l'exécution à une adresse donnée, alors
que `RET` permet de reprendre l'exécution à la suite de l'instruction
qui a réalisée le branchement.


### → Reprenons $`x^3 = x^2 \times x`$ ###

Proposer le code d'une _procédure_ de calcul du carré d'un entier
donné, et celui d'une seconde procédure du calcul du cube. 

Au delà du mécanisme de `CAL`/`RET`, on notera le nécessaire choix d'une 
adresse pour les "paramètres" et la "valeur de retour".

### → Factorielle ###

Proposer le code d'une procédure de calcul de la factorielle d'un
entier. On s'attachera à proposer une version récursive. 

On bute sur la nécessité d'une case mémoire pour stocker le paramètre
(ou une variable "locale") pour chacune des instances d'appel de la
"fonction".

Paramètres et valeur de retour dans la pile
-------------------------------------------

Afin de réaliser plus simplement et plus efficacement des appels de
fonctions paramétrées, on étend le jeu d'instruction pour permettre

* d'empiler/dépiler - _push_/_pop_ - une valeur sur la pile pointée
  par SP,
  mnémoniques `PSH` et `POP`
* d'accéder - _get SP_, _set SP_ - à des mots mémoire d'adresse
  relative à SP, 
  mnémoniques `GSP` et `SSP`

op0 | op1 op2 | mnémonique | instruction à réaliser
--- | ------- | ---------- | ----------------------
3   | 9 7     | `PSH`      | copie la valeur de R dans la case mémoire SP ; décrémente SP 
3   | 9 6     | `POP`      | incrémente SP ; copie la valeur de la case mémoire SP dans R
3   | 8 _d_   | `SSP`      | copie la valeur de R dans la case mémoire SP+_d_
3   | 7 _d_   | `GSP`      | copie la valeur de la case mémoire SP+_d_ dans R 

### → Convention d'appel ###

Soit une fonction à deux paramètres entiers et retournant une valeur
entière.

* Proposer une organisation de la pile d'appel :
  * quelles sont les valeurs qui doivent être présentes sur la pile
	lors de l'exécution de la fonction
  * quelles sont les instructions que doit réaliser la fonction
	appelante ? vant l'appel ? après le retour de la fonction
	appelée ? 
  * comment la fonction appelée accède-t-elle aux paramètres ? Comment
    positionne-t-elle la valeur de retour ? 

### → Au carré ###

Définir une fonction produit, une fonction carré, et un programme qui
affiche le carré d'un entier saisi au clavier.

Exécuter ce programme. 

### → Factorielle, le retour ###

Définir une fonction factorielle, un programme qui affiche la
factorielle d'un entier saisi au clavier.

Exécuter ce programme. 

Draft zone
==========

* la translation d'une zone mémoire, par exemple
  * ajouter la valeur 1 aux cases mémoire à partir d'une adresse
	donnée
  * et sur une longueur donnée, ou jusqu'à trouver un marqueur de fin
	(valeur nulle), etc. 
  

<!--  LocalWords:  Quinson _addr_
 -->
