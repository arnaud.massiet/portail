## Illustration des difficultés à définir un invariant dans le langage complet.
## Problème recontré, ici on utilise implicitement un itérateur sur les listes.
## Mais on a pas un accès explicite à la position dans la liste. Du coup, il
## est difficile d'exprimer formellement des propriétés sur l'état du
## programme. Le plus simple ici est de s'en tenir dans un premier temps à une
## approche informelle et d'exprimer l'invariant de la boucle de la fonction
## miroir par quelque chose du genre :
##
## res contient le préfixe parcouru de la liste mais avec les éléments rangés
## en sens inverse.


def miroir(l):
    res=()
    for a in l:
        res = (a)+res
    return res

miroir("abc")
