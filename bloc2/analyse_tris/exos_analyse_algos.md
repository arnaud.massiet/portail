---
title: Exercices analyse de complexité d'algorithmes
author: Équipe pédagogique DIU EIL Lille
date: janvier 2021
geometry: width=18cm, height=25cm
graphics: oui
numbersections: oui
---
**Objectif** réaliser une analyse de complexité théorique d'algorithmes simples.

# Tri à bulle

L'algorithme suivant est un algorithme de tri dénommé *tri à
bulles* qui est une certaine forme de tri par sélection du minimum.

```
Entrées : t une liste de longueur n. 
Sortie : Rien
Effet  : t est une liste triée de longueur n
contenant les mêmes éléments.

Pour i variant de 0 à n-2:
   # mettre le plus petit élément de la tranche t[i:n] en position i
   Pour j variant de n-1 à i+1 en décroissant
      Si t[j] < t[j-1], alors
         échanger t[j] et t[j-1]
      Fin Si
   Fin Pour
   # la tranche t[0:i+1] est triée et ses éléments 
   # sont inférieurs ou égaux à tous les autres éléments de t
Fin Pour
# La tranche t[0:n] est triée
```

1. Donner les états successifs de la liste à la fin de chaque
   itération de la boucle la plus interne, lorsque i=0 et la liste à
   trier est `[ T, I, M, O, L, E, O, N ]`
2. Même question pour la fin de chaque étape de la boucle la plus
   externe.
3. Combien de comparaisons d'éléments de la liste sont-elles effectuées
   lors du tri d'une liste de longueur $n$ ?
4. Si lors d'une étape de la boucle pour la plus externe, aucun
   échange n'est effectué dans la boucle la plus interne, c'est que la
   liste est triée. Il est donc inutile de poursuivre la boucle externe.  
   Décrire un algorithme qui tient compte de cette remarque.
5. Combien de comparaisons d'éléments de la liste sont-elles
   effectuées lors du tri d'une liste de longueur $n$ avec cette
   variante du tri à bulle ? Décrire le meilleur et le pire des cas.
6. Écrire une procédure python qui trie la liste passée en paramètre
   selon cet algorithme.
   

# Calcul de puissances

**Définition :** La taille d'un entier naturel est le nombre minimum de
chiffres nécessaires pour l'écrire en base 2.

**Propriété :** On admet que la taille $t(n)$ de l'entier $n$ est donnée par la
formule : $$t(n)=\left\lfloor \log_2(n)\right\rfloor + 1$$

Une fonction de calcul de la puissance $n$-ième d'un nombre flottant
est réalisée ci-dessous en utilisant deux algos différents. Les deux
réalisations respectent cette documentation :

```python
    '''
    renvoie la puissance n-ième de x
    
    :param x: (int ou float) un nombre
    :param n: (int) un exposant
    :return: (float) puissance n-ieme de x
    :CU: n >= 0
    :Exemples:
    
    >>> puissance(2, 0)
    1.0
    >>> puissance(2, 10)
    1024.0
    '''
```

La question est d'analyser le nombre de multiplications de nombres
flottants et de (tenter de) l'exprimer en fonction de l'exposant $n$.

## Première méthode

Algorithme suivant la définition de la puissance $n$-ième d'un nombre
comme étant ce nombre multiplié par lui-même $n$ fois.


```python
def puissance1(x, n):
    res = 1.
    for _ in range(n):
        res *= x
    return res
```

## Seconde méthode : exponentiation rapide

Cette seconde méthode s'appuie sur la remarque suivante :

$$ x^n = \left\{
\begin{array}{ll}
(x^{2})^p & \mbox{si }n=2p\\
(x^2)^p\times x & \mbox{si }n=2p+1
\end{array}
\right.$$


```python
def puissance2(x, n):
    r, s, k = 1., float(x), n
    while k != 0:
        if k % 2 == 1:
            r *= s
        s *= s
        k //= 2
    return r
```

