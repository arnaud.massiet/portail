def syracuse(n):
    if n % 2 == 0:
        return n // 2
    else:
        return 3*n + 1

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        n = int(sys.argv[1])
    else:
        n = 30
    while n != 1:
        print(n)
        n = syracuse(n)
    print(n)