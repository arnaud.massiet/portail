Groupes
=======

[DIU Enseigner l'informatique au lycée](../Readme.md)
* [promotion 2018/19-2020/21](Readme-1820.md)

Promotion 2020/21-2021/22
-------------------------

* 4 groupes de 12 personnes
  [2020-22-diu-groupes.csv](2020-22-diu-groupes.csv)
* fiche émargement
  [OpenDocument](2020-22-diu-emargement.ods2020-22-diu-emargement.ods)
  / [PDF](2020-22-diu-emargement.pdf)

| nom              | prénom     | groupe |
|------------------|------------|--------|
| ACROUTE | Denis | 1 |
| BAROIS | David | 1 |
| BEAUCOURT | Frédéric | 1 |
| BELIN | Alexis | 1 |
| BOUACHRA | Bachir | 1 |
| BOURRADA | Brahim | 1 |
| CARON | Maxime | 1 |
| CASSEZ | Olivier | 1 |
| COTTEL | Thierry | 1 |
| CRITELLI | Massimo | 1 |
| D AUBUISSON | Stéphane | 1 |
| DELALLEAU | Éric | 1 |
| DESESQUELLES | Francois | 2 |
| DHAUSSY | Gilles | 2 |
| DJAMAI | Benaouda | 2 |
| DUCHEMIN | Frédéric | 2 |
| DUDA | Aurélie | 2 |
| EUDES | Fabrice | 2 |
| FALEMPIN | Émilie Agnès | 2 |
| FROMENTIN | Arnaud | 2 |
| GALLOT | Pierre-François | 2 |
| GERMAIN | Philippe | 2 |
| GHESQUIERE | Cécile | 2 |
| GUILLON | Gaëtan | 2 |
| LAGODZINSKI | Rémi | 3 |
| LE MIEUX | Vincent | 3 |
| LEMAIRE | Mélanie | 3 |
| LIBBRECHT | Pierre | 3 |
| LIBERT | Frédéric | 3 |
| MACKE | Anthony | 3 |
| MARKEY | Benoît | 3 |
| MARTIN | Stephan | 3 |
| MASSIET | Arnaud | 3 |
| MISLANGHE | Alexandre | 3 |
| MISSUWE | Stéphane | 3 |
| MORAGUES | Arnaud | 3 |
| MOUYS | Ludovic | 4 |
| NEUTS | François | 4 |
| NOTREDAME | Nicolas | 4 |
| PLANQUART | Patrick | 4 |
| PRZYSZCZYPKOWSKI | Jean-Marc | 4 |
| QUENTON | Denis | 4 |
| RAMSTEIN | Messaline | 4 |
| RITZ | Florent | 4 |
| ROLLOT | Frédéric | 4 |
| VANROYEN | Jean-Philippe | 4 |
| WIATR | Véronique | 4 |
| WISSART | Rémi | 4 |

<!-- eof --> 
