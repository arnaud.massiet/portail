# Programmation Dynamique

## Petit Projet COVID

Le but de ce TP est d’implanter le calcul de la distance d’édition vu en cours, ainsi que l’alignement de deux mots puis de découvrir les hypothèses sur l'origine du COVID.

1. Compléter la fonction `minimum` qui renvoie le minimum de ses 3 paramètres.

   ```python
   def minimum(a,b,c):
       """
       >>> minimum(1,2,3)
       1
       >>> minimum(2,1,3)
       1
       >>> minimum(2,3,1)
       1
   		"""
   ```

   

2. Compléter la fonction `valeur` qui prend en paramètre  une matrice E et 2 indices i et j et  qui renvoie  E[ i ] [ j ] si i ≥ 0 et j ≥ 0, i+1 si j = −1 et j+1 si i = −1.

   ```python
    def valeur(E,i,j):
    	"""
    	>>> A=[[1,2,3],[2,3,4]]
        >>> valeur(A,1,1)
        3
        >>> valeur(A,-1,1)
        2
        """	
   ```

   

3. Coder la fonction `distanceEdition` pour remplir la matrice ici notée matrice. On utilisera la fonction valeur pérécedemment définie pour éviter d’avoir à tester si i − 1 ou j − 1 est négatif. La fonction retourne la distance d'édition entre les deux mots s1 et s2.

   ```python
    def distanceEdition(s1,s2,matrice):
        """
        >>> s4='AGORRYTNE'
        >>> s3='ALGORITHME'
        >>> m2=initialise_matrice(len(s3),len(s4),0)
        >>> distanceEdition(s3,s4,m2) == 5
        True
        """
        # Remplir ici
        return matrice[len(s1)-1][len(s2)-1]
   ```

   

4. Compléter la fonction `alignement` qui calcule un alignement des deux chaînes s1 et s2 : la fonction doit modifier s1 et s2 en insérant le caractère `-` (tiret) pour marquer une suppression, ou dans s1 pour marquer une insertion. Vous pourrez utiliser une modification de la fonction de distance d'édition (ici appelée `MdistanceEdition`) pour avoir la matrice d'édition remplie.

   Vous ferez des tests sur "aagtagccactag" et "aagtaagct"

   ```python
    def alignement(s1,s2,matrice):
        """
        >>> s4='AGORRYTNE'
        >>> s3='ALGORITHME'
        >>> m2=initialise_matrice(len(s3),len(s4),0)
        >>> m3=MdistanceEdition(s3,s4,m2)
        >>> aligne=alignement(s3,s4,m3)
        >>> aligne[0]
        'ALGORITHME'
        >>> aligne[1]
        'A-GORRYTNE'
        """
   ```

5. Finir de compléter la fonction alignement pour construire la chaîne aligne : celle-ci doit être constituée des quatre caractères `R` (remplacement), `I` (insertion), `S` (suppression) et `|` (barre verticale, pour signifier que les deux lettres alignées sont égales), afin d’indiquer pour chaque caractère des deux chaînes alignées le type de transformation (ou absence de transformation) effectuée.

   Vous ferez des tests via une fonction test qui utilisera les fonctions précédentes 

   1. 'AGORRYTNE' et 'ALGORITHME' donnera l'affichage

      ```shell
       alignement de  ALGORITHME et  AGORRYTNE : 
       ALGORITHME 
       |I|||RRRR| 
       A-GORRYTNE
      ```
      
   2.  "aagtagccactag" et "aagtaagct" donnera l'affichage :

      ```
       alignement de  aagtagccactag et  aagtaagct : 
       aagtagccactag 
       |||||IIRR||II 
       aagta--agct--
      ```

      

6. On va enfin utiliser notre programme de manière utile ! Le dossier `covid` contient les génomes complets de plusieurs coronavirus (au format [FASTA](https://fr.wikipedia.org/wiki/FASTA_(format_de_fichier)) usuellement utilisé pour échanger des données génomiques):

- `2019-nCoV_WH01.fa`, `2019-nCoV_WH03.fa` et`2019-nCoV_WH04.fa` sont des virus prélevés chez des patients chinois lors de l’épidémie de Covid-19 ; 
- `bat-SL-CoVZC45.fa` et `bat-SL-CoVZXC21.fa` sont des virus portés par des chauve-souris ; 
- `MERS-CoV.fa` et `SARS-CoV.fa` sont des virus prélevés chez des patients lors des épidémies de MERS (Moyen-Orient, 2012) et de SRAS (Asie, 2003).

Une fonction de lecture d'un fichier au format FASTA est fourni dans `lecture.py`.

Les séquences génomiques de COV ont toutes une longueur approximative de 30000 nucléotides. Pour comparer deux génomes de virus afin d'estimer leur ressemblance il faut donc calculer la distance d'édition entre les deux génomes. Le calcul, même en utilisant la programmation dynamique, de deux séquences de 30000 caractères consomme beaucoup de temps. 

Nous vous proposons ici une approximation de la distance d'édition entre deux séquences de virus  comme la somme des distances entre les segments successifs de 60 nucléotides correspondant à chaque ligne du fichier FASTA, ce pourquoi la fonction de lecture retourne une liste des lignes du fichier.

   ```python
    def covid(nom_fichier_humain,nom_fichier_animal):
        animal=lecture(nom_fichier_animal)
        humain=lecture(nom_fichier_humain)
        a=len(animal) # nombre de lignes du fichier
        h=len(humain) # nombre de lignes du fichier
        dtot = 0
        for i in range(0,min(a,h)):
            s=humain[i]
            t=animal[i]
            m=initialise_matrice(len(s),len(t),0)
            d=distanceEdition(s,t,m)
            dtot += d
            print("La distance d'edition entre",s, "et ", t, "est de ",d)
            mm=MdistanceEdition(s,t,m)
            aligne=alignement(s,t,mm)
            print("alignement\n",aligne[0],"\n",aligne[2],"\n",aligne[1])
        print("Distance totale : ", dtot)
   
   
    def test_covid():
        animal="covid/bat-SL-CoVZC45.fa"
        humain="covid/2019-nCoV_WH01.fa"
        covid(humain,animal)
   ```

À l’aide de ces fichiers, tenter de déterminer l’origine du virus actuel : mutation à partir d’un virus porté par les chauve-souris, ou mutation à partir d’un virus humain (MERS-CoV ou SARS-CoV) ?

   

    Note. Vous pourrez comparer vos résultats avec l’étude dont sont tirés ces fichiers, disponible à l’adresse https://www.thelancet.com/pdfs/journals/lancet/PIIS0140-6736(20)30251-8.pdf



