#
# Département informatique, Univ. Lille
# DIU Enseigner l'informatique au lycé
#
# janvier 2020
#

#
# Recherche de chemins dans un graphe
#

import dico


def distance(word1,word2):
    """
    >>> distance("bonjour", "bonjour")
    0
    >>> distance("bonjour", "bbojura")
    2
    >>> distance("bonjour", "boajour")
    1
    >>> distance("bonjour", "xxxxxxx")
    7
    """
    if len(word1) != len(word2):
        raise Exception("words must be of same length")
    word1Char = list(word1)
    word2Char = list(word2)
    word1Char.sort()
    word2Char.sort()
    index_in_word1 = 0
    index_in_word2 = 0
    max_len = len(word1)
    result = max_len
    while index_in_word1 < max_len  and index_in_word2 < max_len :
        if word1Char[index_in_word1] < word2Char[index_in_word2]:
            index_in_word1 = index_in_word1 + 1
        elif word2Char[index_in_word2] < word1Char[index_in_word1]:
            index_in_word2 = index_in_word2 + 1
        elif word1Char[index_in_word1] == word2Char[index_in_word2]:
            index_in_word2 = index_in_word2 + 1
            index_in_word1 = index_in_word1 + 1
            result = result - 1
    return result
    
    
    
def neighbourList(word):
    """
    @param word a word
    @return list of words at a distance of 1 from word
    """
    result = []
    for element in dico.DICO:
        if distance(word,element) == 1:
            result.append(element)
    return result


    
def buildSolution(end, parents):
    """
    @param end final word
    @param parents dictionnary whose key is word and value is parent word (None if no parent == starting word)
    @return list of words from starting word (whose parent is None) to end
    """
    solution = []
    current = end
    while current != None:
        solution = [current] + solution
        current = parents[current]
    return solution





    
#stack behaviour - depth-first/prodondeur d'abord
def addCandidateLast(value, candidate):
    """
    adds value at the end of candidate = enqueue (FIFO/queue behaviour)
    """
    return candidate+[value]
#queue behaviour - breadth fisrt/largeur d'abord
def addCandidateFirst(value, candidate):
    """
    adds value at the beginning of candidate = push (LIFO/stack behaviour)
    """
    return [value]+candidate


def solve(start, goal, addCandidate = addCandidateLast):
    """
    find, if it exists, a path in a graph from start to goal, the addCandidate strategy is used to perform depth-first or breadth-first course
    @param start starting word
    @param goal final word
    @param addCandidate : function ( value, list(value) => list(value) ), adds a new candidate word to existing candidates list
                candidate can be added as first or last element, resulting in a depth-first or breadth-first search
                (default is breadth-first strategy)
    @return : None if no solution or list of words from start to goal
    """
    candidates = [start]
    seen = dict()         # dictionnary, key is a graph node, value is 'parent' node
    seen[start] = None
    found = False
    while candidates != [] and not found:
        current = candidates[0]           # select first candidate as next node to explore
        candidates = candidates[1:]
        if current == goal:
            found = True
        else:
            next = neighbourList(current)  #neighbourList()
            for value in next:
                if not value in seen and not value in candidates:
                    seen[value] = current
                    candidates = addCandidate(value, candidates)
    if found:
        return buildSolution(goal, seen)
    else:
        return None
    




def test_me():
    import doctest
    doctest.testmod()

if __name__ == '__main__':
    print ( 'profondeur d\'abord : ' , solve('ours', 'cage', addCandidateFirst) )
    print ( 'largeur d\'abord : ', solve('ours', 'cage', addCandidateLast) )
    
    
