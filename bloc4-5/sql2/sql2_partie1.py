import sqlite3

conn=sqlite3.connect("personne.db")

cur=conn.cursor()

cur.execute('create table personne(nom text primary key, prenom text, age int)')

nom = 'Dupont'
prenom='Paul'
age = 72
cur.execute('insert into personne values (?, ?, ?)', (nom, prenom, age))

cur.execute("insert into personne values ('Smith', 'Bob', 62)")

liste_personnes = [('Durand', 'Sarah', 45),
                ('Cohen', 'Pauline', 13),
                ('Lisso', 'Marcel', 26)]
               
cur.executemany('INSERT INTO personne VALUES (?,?,?)', liste_personnes)
nom1='Martin'
cur.execute('update personne set nom=?, prenom=?, age=? where nom=?', (nom, prenom,age,nom1))

#personnes = [('Durand'),('Cohen'),('Lisso'),]
#cur.executemany("DELETE FROM personne WHERE nom=?",personnes)

cur.execute("SELECT * FROM personne;")
first_personne = cur.fetchone() # récupère la première personne
print(first_personne)

# le nom de la première personne :
print(first_personne[0],first_personne[1])


cur.execute("SELECT * FROM personne;")
trois_personnes = cur.fetchmany(3) # récupère les 3 premières personnes
print(trois_personnes)

for personne in trois_personnes:
    print(personne[0],personne[1])
    
cur.execute("SELECT * FROM personne;")
personnes = cur.fetchall() # récupère toutes les personnes
print(personnes)

# le nom et le prénom de ces personnes :
for personne in personnes:
    print(personne[0],personne[1])


cur.execute("SELECT * FROM personne where prenom=?;",['Bob'])
first_personne = cur.fetchone() # récupère la première personne dont le prénom est Bob
print(first_personne)

# ou en utilisant une variable
prenom='Bob'
cur.execute("SELECT * FROM personne where prenom=?;",[prenom])
first_personne = cur.fetchone() # récupère la première personne dont le prénom est prenom
print(first_personne)

conn.commit()

conn.close()